using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Synchronizer : MonoBehaviour
{
	
	private NetworkView myView;
	
	private Transform myTransform;
	
	private Vector3 position;
	
	private Quaternion rotation;
	
	private BaseController myController;
	
	private List<NetState> stateBuffer;
	
	public int bufferSize = 20;
	
	public float pingMargin = 0.5f;
	
	public int lastStateUsed;
	
	void Awake()
	{
		
		myView = GetComponent<NetworkView>();
		
		myTransform = transform;
		
		myController = GetComponent<BaseController>();
	
		stateBuffer = new List<NetState>();
	}
	
	void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info)
	{
		
		Vector3 pos = myTransform.position;
			
		Quaternion rot = myTransform.rotation;
		
		if(stream.isWriting)
		{
			stream.Serialize(ref pos);
			
			stream.Serialize(ref rot);
			
		}
		else
		{
			
			stream.Serialize(ref pos);
			
			stream.Serialize(ref rot);
			
			position = pos;
			
			rotation = rot;
			
			myController.Synchronize(position, rotation);
			
			stateBuffer.Insert(0, new NetState(info.timestamp, position, rotation));
			
			if(stateBuffer.Count > bufferSize)
			{
				
				stateBuffer.RemoveAt(stateBuffer.Count -1);
				
			}
		}
		
	}
	
	void Update()
	{
		
		if(!myView.isMine)
		{
			lastStateUsed = -1;
			//float clientPing = (Network.GetAveragePing(Network.player) / 100f) + pingMargin;
			
			double clientPing = 0.1;
			
			double interpolationTime = Network.time - clientPing;
			
			if(stateBuffer.Count > 0)
			{
				if(stateBuffer[0].time > interpolationTime)
				{
					for(int i = 0; i < stateBuffer.Count; i++)
					{
						
						if(stateBuffer[i].time < interpolationTime || i == stateBuffer.Count -1)
						{
							lastStateUsed = i;
							
							NetState bestTarget = stateBuffer[Mathf.Max(i-1, 0)];
							
							NetState bestStart = stateBuffer[i];
							
							double timeDifference = bestTarget.time - bestStart.time;
							
							float lerpTime = 0;
							
							if(timeDifference > 0.0001f)
							{
								
								lerpTime = (float) ((interpolationTime - bestStart.time) / timeDifference);
								
							}
							
							myTransform.position = Vector3.Lerp(bestStart.position, bestTarget.position, lerpTime);
							
							myTransform.rotation = Quaternion.Slerp(bestStart.rotation, bestTarget.rotation, lerpTime);
						
							break;
						}
						
						
					}
				}
				else
				{
					
					lastStateUsed = 0;
					
					NetState latest = stateBuffer[0];
					
					myTransform.position = Vector3.Lerp(myTransform.position, latest.position, 0.5f);
					myTransform.rotation = Quaternion.Slerp(myTransform.rotation, latest.rotation, 0.5f);
				}
				
			}
			
		}
	
		
		
	}
}

