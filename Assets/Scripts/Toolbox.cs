using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//using System.Text.RegularExpressions;

using System.Text;

public static class Toolbox {

	public enum ParseOptions { ParseEntities, ParseComponent };
	
	private static string[] lookupTable;
	
	public static bool ArrayContains(string[] array, string needle)
	{
		foreach(string str in array)
			if(str == needle)
				return true;
		
		return false;
	}
	
	public static T GetWeightedItem<T>(this ICollection<T> list, float[] weights)
	{
		float random = Random.value;
		float current = 0f;
		int index = 0;
		for(IEnumerator en = list.GetEnumerator(); en.MoveNext();)
		{
			if(index == weights.Length)
			{
				Debug.LogError("Not enough weight indices");
				return default(T);
			}
			current += weights[index++];
			
			if(random < current)
				return (T) en.Current;
		}
		
		Debug.LogError("Combined probability was smaller than 1");
		return default(T);
	}
	
	public static string GetParameters(string line)
	{
		return line.Substring(line.IndexOf(":")+1).Trim();
	}
	
	public static int ParseCoordString(string target, bool isX, out bool center)
	{
		int res = 0;
		
		center = false;
		
		target = target.ToLower();
		
		if(target.IndexOf("center") != -1)
		{
			center = true;
			if(isX)
			res  = Screen.width / 2;
			else
				res = Screen.height / 2;
			
			if(target.IndexOfAny(new char[] { '+', '-' }) != -1)
			{
				int calc = -1;
				
				bool substract = false;
				calc = target.IndexOf("+");
				
				if(calc == -1)
				{
					calc = target.IndexOf("-");
					substract = true;
				}
				center = true;
				
				string valueString = target.Substring(calc +1);
				
				int value = int.Parse(valueString.Trim());
				
				if(!substract)
					res += value;
				else res -= value;
			}
	
		
		}
		else 
		{
			res = int.Parse(target);
		}
		
		return res;
	}
	public static string ConvertToString(string entryName, Hashtable ht)
	{
		
		string str = ""; 
		
		// Get the enumerator for the Hashtable
		IDictionaryEnumerator en = ht.GetEnumerator();		
		
		// Use the enumerator to loop through its contents
		while(en.MoveNext())
		{
			// enumerator.key contains the current key
			string key = en.Key.ToString();
			
			// enumerator.value is the value attached to that key
			
			if(en.Value == null)
			{
				Debug.LogError(en.Key.ToString() + " is null!");
			}
			
			string value = en.Value.ToString();
			
			
			
			if(key.IndexOf("@") == 0)
			{
				str += key + "\n" + value + "\n/" + key.Substring(1) + "\n";
			}
			else str += "\t" + key + " = " + value + "\n";
			
		}
		
		
		
		return str;
	}
	
	public static string CreateVariableList(Dictionary<string, string> hashtable)
	{
		string result = "";
		foreach(string key in hashtable.Keys)
		{
			result += key + "=" + hashtable[key] + ";";
		}
		
		return result;
	}
	
	/*public static Hashtable ParseVariableList(string list)
	{
		Hashtable hashtable = new Hashtable();
		
		string[] items = list.Split(";"[0]);
		
		foreach(string item in items)
		{
			if(item.IndexOf("=") != -1)
			{
				string key = item.Substring(0, item.IndexOf("=")).Trim();
				string value = item.Substring(item.IndexOf("=") +1).Trim();
			
				hashtable[key] = value;
			}
		}
		
		return hashtable;
	}*/
	
	public static Dictionary<string, string> ParseVariableList(string list)
	{
		Dictionary<string, string> dictionary = new Dictionary<string, string>();
		
		string[] items = list.Split(";"[0]);
		
		foreach(string item in items)
		{
			if(item.IndexOf("=") != -1)
			{
				string key = item.Substring(0, item.IndexOf("=")).Trim();
				string value = item.Substring(item.IndexOf("=") +1).Trim();
			
				dictionary.Add(key, value);
			}
		}
		
		return dictionary;
	}
	
	public static string CreateNumberPairList(string[] titles, int[] numbers)
	{
		string result = "";
		for(int i = 0; i < titles.Length; i++)
		{
			result += titles[i] + " = " + numbers[i];
			
			if(i < titles.Length) result += ", ";
		}
		
		return result;
	}
	
	public static void ParseNumberPairList(string list, out string[] titles, out int[] numbers)
	{
		//Debug.Log(list);
		
		if(list.Trim() == "") 
		{
			titles = null;
			numbers = null;
			return;
		}
		string[] items = list.Split(","[0]);
		
		
		titles = new string[items.Length];
		numbers = new int[items.Length];
		
	
		
		for(int i = 0; i < items.Length; i++)
		{
			string data = items[i].Trim();
			
			if(data == "") continue;
			
			string key = data.Substring(0, data.IndexOf("=")).Trim();
							
			string value = data.Substring( data.IndexOf("=")+1 ).Trim();
			
			titles[i] = key;
			numbers[i] = int.Parse(value);
		}
	}
	
	public static string MapToString(int[,] map)
	{
		System.GC.Collect();
		
		int width = map.GetLength(0);
		int height = map.GetLength(1);
				
		StringBuilder builder = new StringBuilder(width * height * 2 + 10);
		
		builder.Append(width + " " + height + " ");
		               
		for(int y = 0; y < height; y++)
			for(int x = 0; x < width; x++)
			{
				builder.Append(LookUp(map[x, y]));
				builder.Append(' ');
			}
			
		return builder.ToString();
	}
	
	public static string LookUp(int number)
	{
		if(number > 99 || number < 0) return number.ToString();
		
		if(lookupTable == null)
		{
			lookupTable = new string [100];
			
			for(int i = 0; i < 100; i++)
			{
				lookupTable[i] = i.ToString();
			}
		}
		
		return lookupTable[number];
	}

	public static int[,] ParseMap(string str)
	{
		string[] values = str.Split(" "[0]);
		
		int w = int.Parse(values[0].Trim());
		int h = int.Parse(values[1].Trim());
		
		int[,] map = new int[w,h];
		
		for(int y = 0, c = 0; y < h; y++)
		{
			for(int x = 0; x < w; x++, c++)
			{
				map[x, y] = int.Parse(values[c+2].Trim());
			}
		}
		
		return map;
	}
	
	public static string CreateList(int[] items)
	{
		string[] values = new string[items.Length];
		
		for(int i = 0; i < items.Length; i++)
			values[i] = "" + items[i];
			
		return CreateList(values);
	}
	
	public static string CreateList(MonoBehaviour[] items)
	{
		string[] names = new string[items.Length];
		
		for(int i = 0; i < items.Length; i++)
		{
			if(items[i] != null)
				names[i] = items[i].name;
			else names[i] = "";
		}
		
		return CreateList(names);
	}
	
	public static string CreateList(string[] items)
	{
		return CreateList(items, ",");
	}
	
	public static string CreateList(string[] items, string delimiter)
	{
		string str = "";
		
		for(int i = 0; i < items.Length; i++)
		{
			if(items[i] != null)
			{
				str += items[i];
			
				if(i < items.Length -1) str += delimiter + " ";
			}  else 
			{
				str += " , ";
			}
		}
		
		return str;
	}
	
	
	public static Hashtable ParseString(string source, ParseOptions parseOptions)
	{
		Hashtable result = new Hashtable();
		
		switch(parseOptions)
		{
			case ParseOptions.ParseEntities:
				string[] res = source.Split("\n"[0]);
		
				string entityName = "";
				string entityContents = "";
				foreach(string item in res)
				{
					string data = item.Trim();
					
					if(data.IndexOf("@") == 0 && entityName == "")
					{
						entityName = data.Substring(1);
						//Debug.Log("Found " + entityName);
						entityContents = "";
					}
					else if(data.IndexOf("/") == 0)
					{
						string endCode = data.Substring(1);
						
						if(endCode == entityName)
						{
							if(!result.Contains(entityName))
							{
								result.Add(entityName, entityContents);
							
								entityName = "";
							}
						}
						else entityContents += item + "\n";
						
					}
					else entityContents += item + "\n";
				}					
				
				
			
				break;
				
			case ParseOptions.ParseComponent:
			
				string[] lines = source.Split("\n"[0]);
				
				string currentComponent = "";
				string componentContents = "";
				foreach(string line in lines)
				{
					string data = line.Trim();
					if(data.IndexOf("@") == 0 && currentComponent == "")
					{
						currentComponent = data.Substring(1);
						componentContents = "";
						continue;
					}
					
					if(data.IndexOf("/") == 0 && data.Substring(1) == currentComponent)
					{
						result.Add(currentComponent, componentContents);
						currentComponent = "";
						continue;
					}
					
					if(currentComponent != "")
					{
						componentContents += data+"\n";
					}
					else
					{
						if(data.IndexOf("=") != -1)
						{
							string key = data.Substring(0, data.IndexOf("=")).Trim();
							
							string value = data.Substring( data.IndexOf("=")+1 ).Trim();
							
							result.Add(key, value);
						}
					}
					
				}
			
			break;
				
		}
		
		return result;
	}
	
	public static string[] ParseList(string list)
	{
		return ParseList(list, ",");
	}
	
	public static string[] ParseList(string list, string delimiter)
	{
		string[] values = list.Split(delimiter[0]);
		for(int i = 0; i < values.Length; i++)
			values[i] = values[i].Trim();
			
		return values;
	}
	
	public static int[] ParseIntList(string list)
	{

		string[] values = list.Split(","[0]);
		int[] intValues = new int[values.Length];
		
		for(int i = 0; i < values.Length; i++)
			intValues[i] = int.Parse(values[i].Trim());
			
		return intValues;

		
	}
	public static int ParseInt(string value)
	{
		//Debug.Log(value);
		return int.Parse(value.Trim());
	}
	
	public static float ParseFloat(string value)
	{
		return float.Parse(value.Trim());
	}
	
	public static float[] ParseFloatList(string value)
	{
		string [] values = value.Split(',');
		
		float[] floats = new float[values.Length];
		
		for(int i = 0; i < floats.Length; i++)
		{
			floats[i] = ParseFloat(values[i]);
		}
		
		return floats;
	}
	
	public static Vector3 ParseCoords3(string value)
	{
		value = value.Replace("(", "");
		value = value.Replace(")", "");
		string[] parts = value.Split(","[0]);
		
		return new Vector3(ParseFloat(parts[0]), ParseFloat(parts[1]), ParseFloat(parts[2]));
		
	}
	
	public static Vector2 ParseCoords(string value)
	{
		value = value.Replace("(", "");
		value = value.Replace(")", "");
		string[] parts = value.Split(","[0]);
		
		if(parts[0].Contains("."))
			return new Vector2(ParseFloat(parts[0]), ParseFloat(parts[1]));
		return new Vector2(ParseInt(parts[0]), ParseInt(parts[1]));
	}
	
	public static Color ParseColor(string value)
	{
		
		value = value.Replace("RGBA(", "").Replace(")", "");
		
		float[] colorValues = ParseFloatList(value);
		
		return new Color(colorValues[0], colorValues[1], colorValues[2], colorValues[3]);
		
	}
		
	public static int Parse(System.Object obj)
	{
		if(obj == null) return 0;
		return int.Parse(obj.ToString());
	}
		
	public static Hashtable GetStringAsHashtable(string haystack)
	{
		string[] items = haystack.Split(" ".ToCharArray());
		
		Hashtable pairs = new Hashtable();
		
		//Debug.Log("cutting " + haystack);
		foreach(string item in items)
		{
			//Debug.Log("cutting " + item);

			if(item.IndexOf("=") == -1) continue;
			string k = item.Substring(0, item.IndexOf("="));
			string v = item.Substring(item.IndexOf("=")+1);
			
			//Debug.Log(k + " : " + v);
			pairs.Add(k.Trim(), v.Trim());
		}

		return pairs;
	}
	
	public static int GetAsInt(string key, Hashtable ht)
	{
		if(ht[key] == null) 
		{
			Debug.LogError(key + " is null!");
			return -1;
		}	
		return int.Parse((string) ht[key]);
	}
	
	public static string GetValueFor(string key, string haystack)
	{
		string value = "";
		
		value = (string) GetStringAsHashtable(haystack)[key];
		
		return value;
	}	
	public static Hashtable SplitToKeyValuePairs(string[] keys, string values)
	{
		string[] valueArray = values.Split(";".ToCharArray());
		Hashtable pairs = new Hashtable();
		
		for(int i = 0; i < keys.Length; i++)
		{
			pairs.Add(keys[i], valueArray[i].Trim());
		}
		
		return pairs;
	}
	
	
	
	
}
