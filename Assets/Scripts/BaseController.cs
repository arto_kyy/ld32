using UnityEngine;
using System.Collections;

public abstract class BaseController : MonoBehaviour 
{
	protected CharacterController characterController;
	
	protected GameObject model;
	
	private Animation modelAnimation;
	
	private bool moving;
	
	public float speed = 3;
	
	public float positionErrorThreshold = 0.2f;
	
	protected int life;

	protected Renderer modelRenderer;
	
	private bool activated;
	
	public bool isActive
	{
		get
		{
			return activated;
		}
	}

	private bool lockAnimation;

	[RPC]
	protected void SetAnimationLock(int mode)
	{
		lockAnimation = mode == 1;

		if(GetComponent<NetworkView>().isMine)
		{
			GetComponent<NetworkView>().RPC("SetAnimationLock", RPCMode.Others, mode);
		}
	}

	[RPC]
	public void Activate()
	{
		
		activated = true;
		
		if(GetComponent<NetworkView>().isMine)
		{
			
			GetComponent<NetworkView>().RPC("Activate", RPCMode.Others);
			
		}
	}
	
	[RPC]
	public void Deactivate()
	{
		
		activated = false;

		characterController.enabled = false;
		
		if(GetComponent<NetworkView>().isMine)
		{
			
			GetComponent<NetworkView>().RPC("Deactivate", RPCMode.Others);
			
		}
	}
	
	void Awake()
	{
		model = transform.GetChild(0).gameObject;
		
		modelAnimation = model.GetComponent<Animation>();
		
		modelRenderer = transform.FindComponent<Renderer>();
		
	}
	
	// Use this for initialization
	void Start () 
	{
		characterController = GetComponent<CharacterController>();
		
		OnStart();
		
	}

	public bool IsPlaying(string name)
	{
		return modelAnimation.IsPlaying(name);
	}

	
	[RPC]
	public void StopAnimation(string name)
	{

		modelAnimation.Stop(name);
		
		if(GetComponent<NetworkView>().isMine)
			GetComponent<NetworkView>().RPC("StopAnimation", RPCMode.Others, name);
		
	}

	[RPC]
	public void PlayAnimation(string name)
	{
		if(name != "idle") 
			Debug.Log (name);
		
		modelAnimation.CrossFade(name);
		
		if(GetComponent<NetworkView>().isMine)
			GetComponent<NetworkView>().RPC("PlayAnimation", RPCMode.Others, name);
		
	}
	
	[RPC]
	private void IsMoving(int i)
	{
		if(lockAnimation)
			return;

//		Debug.Log (networkView.viewID + " moving: " +i);
		bool isMoving = i == 1;
		
		if(isMoving)
		{

		}
		else 
		{

		}
		
	}
	
	public void Synchronize(Vector3 position, Quaternion rotation)
	{
		
		float distance = Vector3.Distance(transform.position, position);
		
		if(distance > positionErrorThreshold)
		{
			
			float lerp = ((1 / distance) * speed) / 100f;
			
			transform.position = Vector3.Lerp(transform.position, position, lerp);
			
			transform.rotation = Quaternion.Slerp(transform.rotation, rotation, lerp);
			
		}
	}
	
	// Update is called once per frame
	void Update () {
		
		if(GetComponent<NetworkView>().isMine && activated)
		{
			Vector3 movement = characterController.velocity;
			
			if(movement.sqrMagnitude > 0.05f * 0.05f)
			{
				if(!moving)
				{
					moving = true;
					//modelAnimation.CrossFade("run");
					GetComponent<NetworkView>().RPC("IsMoving", RPCMode.All, 1);
				}
			}
			else if(moving)
			{
				moving = false;
				//modelAnimation.CrossFade("idle");
				GetComponent<NetworkView>().RPC("IsMoving", RPCMode.All, 0);
			}
			
			
			
		}
	}
	
	protected abstract Vector3 OnUpdate();
	
	protected virtual void OnStart()
	{
		
	}
	
	protected abstract void OnDeath();
	
	[RPC]
	private void SyncLife(int life)
	{
		
		this.life = life;
		
		if(life <= 0)
		{
			
			OnDeath();
			
			GetComponent<CharacterController>().enabled = false;
			
		}
	}
	
	[RPC]
	private void OwnerOnHit()
	{
		Debug.Log("Argh, I'm hit!");
		life --;
		
		if(life > 0)
		{
			PlayAnimation("hit");
			StartCoroutine(HitEffect());
		}
		else 
		{
			activated = false;
			
			PlayAnimation("death");
			
			OnDeath();
			

			
			//NetworkViewID viewID = networkView.viewID;
			//Network.Destroy(viewID);
			//Network.RemoveRPCs(viewID);
		}
		
		
	}
	
	
	public void Hit()
	{
		
		if(GetComponent<NetworkView>().isMine)
		{
			OwnerOnHit();
		}
		else
		{
			GetComponent<NetworkView>().RPC("OwnerOnHit", GetComponent<NetworkView>().owner);
		}
		
		
		/*Debug.Log("Argh, I'm hit!");
		life --;
		
		networkView.RPC("SyncLife", RPCMode.Others, life);
		
		if(life > 0)
		{
			PlayAnimation("hit");
			StartCoroutine(HitEffect());
		}
		else 
		{
			PlayAnimation("death");
			
			OnDeath();
			
			GetComponent<CharacterController>().enabled = false;
			
			//NetworkViewID viewID = networkView.viewID;
			//Network.Destroy(viewID);
			//Network.RemoveRPCs(viewID);
		}*/
	}
	
	private IEnumerator HitEffect()
	{
		Color oldColor = modelRenderer.material.color;
		modelRenderer.material.color = Color.white;
		yield return new WaitForSeconds(0.1f);
		modelRenderer.material.color = oldColor;
	}
}
