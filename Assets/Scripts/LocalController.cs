using UnityEngine;
using System.Collections;

public class LocalController : BaseController 
{

	public NetworkPlayer myPlayer;
	
	private bool dead;

	protected override void OnStart ()
	{
		base.OnStart ();

		if(GetComponent<NetworkView>().isMine)
		{
			Activate();
		}

	}

	// Update is called once per frame
	protected override Vector3 OnUpdate () 
	{
		Vector3 result = Vector3.zero;
		
		if(GetComponent<NetworkView>().isMine && !dead)
		{
		}
		
		return result;
	}

	protected override void OnDeath ()
	{
		dead = true;
	}
}
