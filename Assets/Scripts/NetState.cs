using UnityEngine;
using System;

public class NetState
{
	
	public double time;
	
	public Vector3 position;
	
	public Quaternion rotation;
	
	public NetState (double time, Vector3 position, Quaternion rotation)
	{
		
		this.time = time;
		
		this.position = position;
		
		this.rotation = rotation;
		
	}
}


