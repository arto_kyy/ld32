using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ConnectionGUI : MonoBehaviour 
{

	public string remoteIP = "127.0.0.1";
	
	public int remotePort = 25000;
	
	public int listenPort = 25000;
	
	public bool useNAT = false;
	
	public string yourIP = "";
	
	public int yourPort = 0;
	
	public GameObject player;
	
	private int numberOfPlayers = 2;
	
	private string numberOfPlayersString = "2";
	
	private int connectedPlayers;
	
	private bool waitingForPlayers;
	
	private List<NetworkPlayer> players;
	
	private NetworkConnectionError connectionError = NetworkConnectionError.NoError;
	
	void OnFailedToConnect(NetworkConnectionError connectionError)
	{
		
		this.connectionError = connectionError;
		
	}
	
	void OnGUI()
	{
		
		if(Network.peerType == NetworkPeerType.Disconnected)
		{
			GUILayout.BeginArea(new Rect(10, 10, 200, 200));
									
			GUILayout.BeginHorizontal();
			
			GUILayout.Label("# of Players:");
			
			numberOfPlayersString = GUILayout.TextField(numberOfPlayersString);
			
			int temp;
			
			if(int.TryParse(numberOfPlayersString, out temp))
			{
				
				numberOfPlayers = temp;
				
			}
			
			if(GUILayout.Button("Start Server"))
			{
				Network.InitializeServer(32, listenPort, useNAT);
				
				//Instantiate(map);
				
			}
			
			GUILayout.EndHorizontal();
			
			GUILayout.BeginHorizontal();
			
			useNAT = GUILayout.Toggle(useNAT, "Use NAT");
			
			GUILayout.EndHorizontal();
									
			GUILayout.BeginHorizontal();
			
			remoteIP = GUILayout.TextField(remoteIP);
			remotePort = int.Parse(remotePort.ToString());
			
			if(GUILayout.Button("Connect"))
			{
				
				 Network.Connect(remoteIP, remotePort);
				
			}
			
			GUILayout.EndHorizontal();
			
			GUILayout.BeginHorizontal();
			
			
			if(GUILayout.Button("Single Player"))
			{
				Network.InitializeServer(32, listenPort, useNAT);

				StartGame(Network.player);
			}
			
			
			GUILayout.EndHorizontal();
			
			GUILayout.EndArea();
			
			if(connectionError != NetworkConnectionError.NoError)
			{
				
				GUILayout.BeginArea(new Rect(Screen.width / 2 - 200, Screen.height - 350, 400, 300));
				
				GUILayout.Box ("Connection Error");
				
				GUILayout.Label(connectionError.ToString());
				
				GUILayout.EndArea();
				
			}
			
			
		}
		else
		{
			yourIP = Network.player.ipAddress;
			yourPort = Network.player.port;
			
			GUILayout.BeginArea(new Rect(10, 10, 200, 200));
			
			GUILayout.BeginHorizontal();
			
			GUILayout.Label("IP Address: " + yourIP + ": " + yourPort);
			
			GUILayout.EndHorizontal();
			
			if(waitingForPlayers)
			{
				GUILayout.BeginHorizontal();
			
				GUILayout.Label("Waiting for players. " + players.Count + " / " + numberOfPlayers + " connected.");				
			
				GUILayout.EndHorizontal();
			}
			GUILayout.BeginHorizontal();
			
			if(GUILayout.Button("Disconnect"))
			{
				Network.Disconnect(200);

				Application.LoadLevel(Application.loadedLevelName);
			}
			
			GUILayout.EndHorizontal();
			
			GUILayout.EndArea();
			
		}
		
	}
	
	void OnServerInitialized()
	{
		players = new List<NetworkPlayer>();
				
		players.Add(Network.player);
		
		waitingForPlayers = true;
				
	}
	
	void OnPlayerConnected(NetworkPlayer player)
	{
		
		players.Add(player);
		
		if(players.Count == numberOfPlayers)
		{
			foreach(NetworkPlayer p in players)
			{
				if(p != Network.player)
				{
					GetComponent<NetworkView>().RPC("StartGame", p, Network.player);
				}
			}

			StartGame(Network.player);
			
		}
		
	}
	
	[RPC]
	public void StartGame(NetworkPlayer serverPlayer)
	{
		Debug.Log ("Start Game");

		GameObject player = (GameObject) Network.Instantiate(this.player, Vector3.zero, Quaternion.identity, 0);

		player.GetComponent<LocalController>().myPlayer = Network.player;

		player.GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().enabled = true;
		player.GetComponentInChildren<Camera>().enabled = true;
	}
	
	
	void OnConnectedToServer()
	{
		// Instantiate(map);

		Debug.Log ("On Connected to server");

		/*GameObject player = (GameObject) Network.Instantiate(this.player, Vector3.zero, Quaternion.identity, 0);
		
		player.GetComponent<LocalController>().myPlayer = Network.player;

		player.GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().enabled = true;*/
	}
}
