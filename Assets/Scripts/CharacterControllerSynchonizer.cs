using UnityEngine;
using System.Collections;

public class CharacterControllerSynchonizer : MonoBehaviour
{
	
	private NetworkView myView;
	
	private Transform myTransform;
	
	private CharacterController myController;
	
	private Vector3 position;
	
	private Quaternion rotation;
	
//	private Vector3 velocity;
	
	void Awake()
	{
		
		myView = GetComponent<NetworkView>();
		
		myTransform = transform;
		
		myController = GetComponent<CharacterController>();
		
	}
	
	void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info)
	{
		
		Vector3 pos = myTransform.position;
			
		Quaternion rot = myTransform.rotation;
			
		Vector3 velocity = myController.velocity;
		
		if(stream.isWriting)
		{
			stream.Serialize(ref pos);
			
			stream.Serialize(ref rot);
			
			stream.Serialize(ref velocity);
		}
		else
		{
			
			Debug.Log ("Reading data");
			stream.Serialize(ref pos);
			
			stream.Serialize(ref rot);
			
			stream.Serialize(ref velocity);
			
			position = pos;
			
			rotation = rot;
			
			//this.velocity = velocity;
			
			myTransform.position = position;
		
			myTransform.rotation = rotation;
			
			myController.Move(velocity);
		}
		
	}
	
	void Update()
	{
		if(!myView.isMine)
		{
			
		}
		
	}

}

